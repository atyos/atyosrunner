import logging
import os

import process
from typing import Dict, List
from termcolor import cprint

from cli.Component import Component, Command
from cli.log import cli_print
from cli.parameters import key, key_bucket_name, key_aws_region, \
    get_and_assert_additional_parameters, key_stack_name, key_prompt
from cli.theme import theme_error0, theme_success0, theme_warn0, theme_main3


def deploy_cloudformation_stack(params: Dict[str, str], component: Component, command: Command) -> int:

    logger = logging.getLogger()

    # USAGE
    all_params = get_and_assert_additional_parameters(
        initial_params=params,
        usage=lambda cn=component.name, cd=command: component.print_command_usage(cn, cd),
        new_required_params=[
            {key: key_bucket_name},
            {key: key_aws_region},
            {key: key_stack_name},
            {key: key_prompt}
        ])

    assert_dependencies(["sam"])

    # EXECUTION
    cloudformation_directory = f"{component.directory}/cloudformation"
    initial_template = f"{cloudformation_directory}/template.yaml"
    sam_build_directory = f"{cloudformation_directory}/.aws-sam/build"
    template_file = f"{sam_build_directory}/template.yaml"

    # BUILD
    sam_build, _, _ = process.run(
        [
            f"sam build"
            f"  --template {initial_template}"
            f"  --build-dir {sam_build_directory}"
        ], logger)

    if sam_build.returncode != 0:
        logger.error("Unable to run sam build command")
        exit(1)

    # DEPLOY
    os.environ['AWS_DEFAULT_REGION'] = all_params[key_aws_region]
    
    if all_params[key_prompt] != "bypass":
        sam_deploy, _, _ = process.run(
            [
                f"sam deploy"
                f"  --template-file {template_file}"
                f"  --stack-name {all_params[key_stack_name]}"
                f"  --s3-bucket {all_params[key_bucket_name]}"
                f"  --capabilities CAPABILITY_IAM CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND"
                f"  --no-fail-on-empty-changeset"
                f"  --no-execute-changeset"
            ], logger)

        cli_print('Do you want to apply the above changeset ?', theme_warn0)
        answer = input("[y/n]: ")
        if answer != "y":
            exit(1)

    sam_deploy, _, _ = process.run(
        [
            f"sam deploy"
            f"  --template-file {template_file}"
            f"  --stack-name {all_params[key_stack_name]}"
            f"  --s3-bucket {all_params[key_bucket_name]}"
            f"  --capabilities CAPABILITY_IAM CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND"
            f"  --no-fail-on-empty-changeset"
        ], logger)

    if sam_deploy.returncode != 0:
        logger.error("Unable to run sam build command")
        exit(1)

    return 0


def assert_dependencies(dependencies: List[str]):
    for command in dependencies:
        _, stdout, stderr = process.run([f'command -v {command}'])
        if stdout == "":
            cprint(f"Error, command {command} is required", theme_error0)
            exit(1)
