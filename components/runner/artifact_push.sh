set -eu

SEMANTIC_VERSION=${1:?SEMANTIC_VERSION parameter is needed as 1st parameter}
FULL_SEMANTIC_VERSION=${2:?FULL_SEMANTIC_VERSION parameter is needed as 2nd parameter}
PIPELINE_ID=${3:?PIPELINE_ID parameter is needed as 3rd parameter}
BRANCH=${4:?BRANCH parameter is needed as 4th parameter}
SHORT_SHA=${5:?SHORT_SHA parameter is needed as 5th parameter}


sed -i "s/__SEMANTIC_VERSION__/${FULL_SEMANTIC_VERSION}/g" bootstrap.sh

(
    cd src || exit 1
    zip_file=runner.zip
    base_path=s3://runnerartifacts
    zip -r "${zip_file}" .

    # Zip is only stored to the full semantic version that is defined in bootstrap.sh
    echo -e "\n\e[36mPushing FULL_SEMANTIC_VERSION\e[39m"
    original_bootstrap="${base_path}/${FULL_SEMANTIC_VERSION}/bootstrap.sh"
    original_do="${base_path}/${FULL_SEMANTIC_VERSION}/do"
    aws s3 cp "${zip_file}" "${base_path}/${FULL_SEMANTIC_VERSION}/runner.zip"
    aws s3 cp "../bootstrap.sh" "${original_bootstrap}"
    aws s3 cp "../do" "${original_do}"

    # We set a version of do and bootstrap to be able to download them
    echo -e "\n\e[36mDuplicate do and bootstrap as pointers\e[39m"
    for sub_path in "${SEMANTIC_VERSION}" "${PIPELINE_ID}" "${BRANCH}" "${SHORT_SHA}"
    do
        aws s3 cp "${original_bootstrap}" "${base_path}/${sub_path}/bootstrap.sh"
        aws s3 cp "${original_do}" "${base_path}/${sub_path}/do"
    done
)
