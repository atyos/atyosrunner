#!/usr/bin/env sh

semantic_version=$(cat src/version)
docker build \
    -t "registry.gitlab.com/atyos/atyosrunner:${semantic_version}" \
    .
