#!/usr/bin/env sh

semantic_version=$(cat src/version)
image="registry.gitlab.com/atyos/atyosrunner:${semantic_version}"
image_build="${image}.${CI_PIPELINE_ID}"
docker push "${image}"
docker tag "${image}" "${image_build}"
docker push "${image_build}"
