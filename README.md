# Atyos Runner

You want to:
- install and run:
  ```shell script
  curl -O https://runnerartifacts.s3.eu-west-3.amazonaws.com/master/do
  chmod +x do 
  ./do
  ```

- understand:
    - [concepts](docs/README.md)

- dev:
    - [setup environment for development](docs/setup_environment_for_development.md)

- A new build is generated each time a commit is pushed to gitlab
