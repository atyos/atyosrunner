# Setup Environment for Development

## Common
- `git clone git@gitlab.com:atyos/atyosrunner.git` or `git clone https://gitlab.com/atyos/atyosrunner.git`

## JetBrains:  Intellij / PyCharm
- Create project: `File --> New --> Project from existing sources ...`, select cloned directory
- Setup SDK: `File --> Project Structure --> SDKs --> + --> Add Python SDK --> Virtualenv --> OK`

## Debug mode
The AtyosRunner proposes a debug mode to use your local AtyosRunner repository
with a do command inside another repository

As an example:
- you have cloned the Atyos repository at `/repos/AtyosRunner`
- you are working on a project at `/repos/myProject`
    - you have installed the do at `/repos/myProject/do`

You are then able to execute a do command with the local repo like this:
```shell
cd /repos/myProject
RUNNER_EXEC_DIR=/repos/AtyosRunner/components/runner \
./do --host --compoenent my_coponent --command my_command
```
