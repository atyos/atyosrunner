# Features

## Versioning, Release, Update
This is mandatory for people to feel not wasting time on something never used.
It's probably easy to set up if we leverage Docker.

## Reference channel instead of version
To ease usage of the tool, we can have a channel configuration (like dev) instead
of using a version in the configuration. A channel act as a pointer and
can result in different behaviour facilitating the usage of the tool.

## Variable injection
The idea here is to reduce a big quantity of the "glue" code written everywhere.
As usually, this one consist 50% of passing or validating parameters.

The pattern could be, in a file, you define directly some variables.
The tool will then automatically validate if these one are present.
(see hive project)
```shell script
id="<% cli.id %>"
container="<% gcloud.image %>:<% gcloud.major %>.<% gcloud.minor %>"
```
- `cli.id` means you need a parameter --id passed the cli
- `gcloud.image` refer to a parameter from the configuration file.

## Bootstrap
We can use the tool to store the templates that we think are relevant
for use in future projects. It would probably act as some documentation.

## Components
Organizing code as components seems a way to ease organizing
hierarchical elements. The components refer between them through the
configuration, but the deployment is not opinionated of that structure.

## Automatic discovery of usage and commands
