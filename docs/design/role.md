# Role

## Share practices between projects
Project tends to isolate group of people. We focus on current tasks
and ideas stay inside projects. Good idea emerge but are hard to
transfer and explain as best practice in other ones as other people
have not encountered the same problematics.

## Integrate at the different level of the application
This could provide features at any level of the application.
Example would be code simplification but also feature hard to reproduce
due to lack of time.

## Climb the ladder
By iterating the development across multiple projects, we should be able
to not rewrite each time basic feature (logging, metric, glue etc)
