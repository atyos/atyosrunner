# CHANGELOG
## 1.5.1
- Add CAPABILITY_AUTO_EXPAND to allow nested stacks deployment
 
## 1.5.0
- Add PyEnv and python3.9

## 1.4.3
- Remove unnecessary duplicated deploy if "--prompt bypass" flag is provided

## 1.4.1
- usage
  - commands are now sorted
  - columns will be adapted to names
- only commands starting with `runner.` will be executed
- container will now have minimum set of ENV:
    - RUNNER_PROJECT_DIR
    - RUNNER_COMPONENTS_DIR
    - RUNNER_DOTENV_DIR 
    - RUNNER_HOME 
- RUNNER_DOTENV_DIR will point to /project (it was initially pointed to /runner)

## 1.4.0
- run multiple times the same command in parallel
- usage
    - example will print correctly
    - usage won't duplicate multiple times the same parameters
    - commands are now sorted
    - columns will be adapted to names
- clean not used files or code
- ease debug with a log.debug_print
- correctly print error when component doesn't exist
- only commands starting with `runner.` will be executed

## 1.3.0
- handle multiple dot.*.env files
- check do version and exit on incompatibility
- host mode will now handle do.*.env files

## 1.2.0
- add internal commands
- add binary component

## 0.3.6
- kubectl: add kubeconfig support for docker

## Not released
